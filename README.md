# Chess-Project

Chess developed in the second year of Magshimim.
The project includes a comfortable user interface.

By Alon Kopilov, Osher Malka, Niv Ben Harush.

*HOW TO RUN?*
1. Open "chessGraphics.exe". That'll open the User Interface
2. Open "chess_project_solution" in the folder by the same name
3. Build and run.

