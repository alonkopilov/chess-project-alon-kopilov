#pragma once

#include "ChessBoard.h"

#define DEFAULT_CHESS_PLACEMENT "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"
#define BLACK_PLAYER 'b'
#define WHITE_PLAYER 'w'

// ATE = A MOVEMENT
#define ATE_MOVE_SAME_INDEX 7
#define ATE_NULL_PIECE 2
class Game
{
public:
	/*
		Constructs a Chess game by building a Chess board, and setting the starting player
	*/
	Game();

	/*
		Distructs a chess game by deleting the board
	*/
	~Game();

	/*
		Gets a FOUR LETTER location string in which the first two characters are the position of the piece we want to move
		and the last two characters are the destination square. The function then performs the move in ChessBoard
		input: std::string four letter location
		output: int move result code
	*/
	int move(const std::string& location);

	/*
		Swaps the player turns.
	*/
	void swapTurns();

	/*
		Returns a string of the board representation
		output: std::string board
	*/
	std::string printGameBoard();

private:
	ChessBoard* _board;
	char _turn;
};

