#include "Bishop.h"

/*
A constructor of Bishop, builds new Bishop.
input: type, color and location.
output: none.
*/
Bishop::Bishop(const char& type, const char& color, const std::string& location) : ChessPiece(type, color, location)
{
}

/*
	Moves a Bishop based on a given location string
	input: string newLocation (4 LETTERS), pointer to ChessPiece double array (the chess board)
	output: result of eating (or not) code. returns 6 if the move was invalid
*/
int Bishop::move(std::string& newLocation, ChessPiece* board[][BOARD_LEN])
{
	int newX = this->getX(newLocation[DEST_X_INDEX]);
	int newY = this->getY(newLocation[DEST_Y_INDEX]);
	int currX = this->getX(this->getLocation()[SOURCE_X_INDEX]);
	int currY = this->getY(this->getLocation()[SOURCE_Y_INDEX]);
	int eatCode = ATE_INVALID_MOVE, directionX = 0, directionY = 0, i = 0; //-1 default value

	if (abs(newX - currX) == abs(newY - currY)) //Check if the move is diagonal
	{
		directionX = (newX - currX) / abs(newX - currX); //Get the X direction in the board ('-1' left, '1' right)
		directionY = (newY - currY) / abs(newY - currY); //Get the Y direction in the board ('-1' up, '1' down)

		for (i = 1; i < abs(newX - currX); i++)
		{
			if (board[currY + (i * directionY)][currX + (i * directionX)] != NULL &&
				board[currY + (i * directionY)][currX + (i * directionX)] != this) //Check if there is another player in the way
			{
				return ATE_INVALID_MOVE; //Invalid move (player in the way)
			}
		}
		eatCode = eatChessPiece(newLocation, board);
	}
	else
	{
		return ATE_INVALID_MOVE; //Invalid move (not a diagonal move)
	}

	return eatCode;
}