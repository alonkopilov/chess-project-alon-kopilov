#include "King.h"

/*
A constructor of King, builds new King.
input: type, color and location.
output: none.
*/
King::King(const char& type, const char& color, const std::string& location) : ChessPiece(type, color, location)
{
}

/*
	Moves a King based on a given location string
	input: string newLocation (4 LETTERS), pointer to ChessPiece double array (the chess board)
	output: result of eating (or not) code. returns 6 if the move was invalid
*/
int King::move(std::string& newLocation, ChessPiece* board[][BOARD_LEN])
{
	int newX = this->getX(newLocation[2]);
	int newY = this->getY(newLocation[3]);
	int currX = this->getX(this->getLocation()[0]);
	int currY = this->getY(this->getLocation()[1]);
	int eatCode = 0;
	if (0 <= newX < BOARD_LEN && 0 <= newY < BOARD_LEN
		&& (newX + 1 == currX || newX - 1 == currX || (newX == currX && newY != currY)) // allows on one index of movement
		&& (newY + 1 == currY || newY - 1 == currY || (newY == currY && newX != currX)))//make better
	{
		eatCode = eatChessPiece(newLocation, board);
		return eatCode;
	}
	return ATE_INVALID_MOVE;
}