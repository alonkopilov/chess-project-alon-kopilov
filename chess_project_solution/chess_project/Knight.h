#pragma once

#include "ChessPiece.h"

class Knight : public ChessPiece
{
public:
	/*
	A constructor of Knight, builds new Knight.
	input: type, color and location.
	output: none.
	*/
	Knight(const char& type, const char& color, const std::string& location);
	/*
	Moves a Knight based on a given location string
	input: string newLocation (4 LETTERS), pointer to ChessPiece double array (the chess board)
	output: result of eating (or not) code. returns 6 if the move was invalid
	*/
	virtual int move(std::string& newLocation, ChessPiece* board[][BOARD_LEN]);
};

