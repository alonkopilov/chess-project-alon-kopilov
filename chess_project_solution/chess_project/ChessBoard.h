#pragma once

#include <iostream>
#include <string>
#include "ChessPiece.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Rook.h"
#include "Queen.h"

#define LOCATION_X_INDEX 0
#define LOCATION_Y_INDEX 1
#define FIRST_CHAR_ASCII 'a'
#define FIRST_INT_ASCII '0'
#define WHITE_QUEEN_CHAR 'Q'
#define BLACK_QUEEN_CHAR 'q'
#define BOARD_SIZE 8
#define EMPTY_SQUARE '#'

enum CHESS_PIECES {KING = 'k', QUEEN = 'q', ROOK = 'r', KNIGHT = 'n', BISHOP = 'b', PAWN = 'p'};

class ChessBoard
{
public:
	/*
		Gets a board string format and constructs a new board by taking every char in the boardFormat and
		allocating a new chess piece by its type
		input: std::string board format
	*/
	ChessBoard(const std::string& boardFormat);

	/*
		Distructs the chess board by deallocating all chess pieces
	*/
	~ChessBoard();


	int move(const std::string& location);

	/*
		Gets a TWO LETTER location string and checks if there is a piece in that location on the board
		input: std::string location string
		output: the chess piece object in that location
	*/
	ChessPiece* playerInLocation(const std::string& location) const;

	/*
		Creates a string representation of the chess board with all pieces
		output: std::string board representation
	*/
	std::string printBoard() const;

	//Board Getters
	/*
		Gets a color of the player and checks if that player is on a Check
		input: char player color
		output: true if the player is on Check, else false
	*/
	bool isCheckOnColor(char color);

	/*
		Gets a char X location and returns it's representation on the board
		input: char location
		output: int representation on the board
	*/
	int getX(char location) const;

	/*
		Gets a char Y location and returns it's representation on the board
		input: char location
		output: int representation on the board
	*/
	int getY(char location) const;

	/*
		Gets a TWO CHAR location and returns the color of the piece in that location
	*/
	char getColor(std::string location) const;

private:	
	ChessPiece* _board[BOARD_SIZE][BOARD_SIZE];

	//helpers
	/*
		Gets a location string and checks if it is a valid location on the board
		input: std::string location
		output: true if the location is valid, else false.
	*/
	bool isLocationValid(std::string location);

	/*
		Returns the king piece of a given player color
		input: char player color
		output: king piece of that color
	*/
	ChessPiece* getKingToAttack(char color);

	/*
		Build a new chess piece from parameters
		input: char piece char (for example: 'r' for black rook), int X location on the board, int Y location on the board
	*/
	ChessPiece* buildChessPiece(char pieceChr, int x, int y);

	/*
		Returns the color of the chess piece (uppercase piece char = white, lowercase piece char = black)
		output: char piece color
	*/
	char findPieceColor(char pieceChr);

	/*
		Gets the X and Y location of the piece in the board and builds a string representing the piece location
		input: int X location, int Y location
		output: std::string location string
	*/
	std::string buildPieceLocationString(int x, int y);
};

