#include "Knight.h"

/*
A constructor of Knight, builds new Knight.
input: type, color and location.
output: none.
*/
Knight::Knight(const char& type, const char& color, const std::string& location) : ChessPiece(type, color, location)
{

}

/*
	Moves a Knight based on a given location string
	input: string newLocation (4 LETTERS), pointer to ChessPiece double array (the chess board)
	output: result of eating (or not) code. returns 6 if the move was invalid
*/
int Knight::move(std::string& newLocation, ChessPiece* board[][BOARD_LEN])
{
	int newX = this->getX(newLocation[DEST_X_INDEX]);
	int newY = this->getY(newLocation[DEST_Y_INDEX]);
	int currX = this->getX(this->getLocation()[SOURCE_X_INDEX]);
	int currY = this->getY(this->getLocation()[SOURCE_Y_INDEX]);
	int eatCode = -1; //-1 default value

	//Checks all possible valid movement combinations that the Knight can do
	if ((abs(newX - currX) == 2 && abs(newY - currY) == 1) || (abs(newX - currX) == 1 && abs(newY - currY) == 2))
	{ // checks if a valid knight move is being done, knight moves 2 to the front and one to the side
		eatCode = eatChessPiece(newLocation, board);
	}
	else
	{
		eatCode = ATE_INVALID_MOVE; //Invalid move
	}
	return eatCode;
}