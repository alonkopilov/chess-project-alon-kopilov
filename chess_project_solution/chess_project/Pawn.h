#pragma once

#include "ChessPiece.h"
#define FIRST_MAX_MOVE 2
#define MAX_MOVE 1
class Pawn : public ChessPiece
{
public:
	/*
	A constructor of Pawn, builds new Pawn.
	input: type, color and location.
	output: none.
	*/
	Pawn(const char& type, const char& color, const std::string& location);
	/*
	Moves a Pawn based on a given location string
	input: string newLocation (4 LETTERS), pointer to ChessPiece double array (the chess board)
	output: result of eating (or not) code. returns 6 if the move was invalid
	*/
	virtual int move(std::string& newLocation, ChessPiece* board[][BOARD_LEN]);

private:
	bool _did_moving;
};

