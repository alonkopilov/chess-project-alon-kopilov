#include "Pawn.h"

/*
A constructor of Pawn, builds new Pawn.
input: type, color and location.
output: none.
*/
Pawn::Pawn(const char& type, const char& color, const std::string& location) : ChessPiece(type, color, location)
{
	this->_did_moving = false;
}

/*
	Moves a Pawn based on a given location string
	input: string newLocation (4 LETTERS), pointer to ChessPiece double array (the chess board)
	output: result of eating (or not) code. returns 6 if the move was invalid
*/
int Pawn::move(std::string& newLocation, ChessPiece* board[][BOARD_LEN])
{
	int newX = this->getX(newLocation[DEST_X_INDEX]); //new X value
	int newY = this->getY(newLocation[DEST_Y_INDEX]); //new Y value
	int currX = this->getX(this->getLocation()[SOURCE_X_INDEX]); // current X value
	int currY = this->getY(this->getLocation()[SOURCE_Y_INDEX]); // current Y value
	int eatCode = ATE_INVALID_MOVE; // default value

	//if moving up -- positive
	//if moving down -- negative
	if (MIN_BOARD_INDEX > newX || newX > MAX_BOARD_INDEX || MIN_BOARD_INDEX > newY || newY > MAX_BOARD_INDEX) // invalid indexes
	{
		return ATE_INVALID_MOVE;
	}
	//FOLLOWING LINE HELPS CALCULATING
	int moveOnY = newY - currY;
	
	if(this->getColor() == BLACK_COLOR_CHAR && moveOnY > 0 ||
		this->getColor() == WHITE_COLOR_CHAR && moveOnY < 0) // checking if the black or white, needs to move according to the color not the opposite direction.

	if (!this->_did_moving && (moveOnY > FIRST_MAX_MOVE || moveOnY < -FIRST_MAX_MOVE) ||
		_did_moving && (moveOnY > MAX_MOVE || moveOnY < -MAX_MOVE)) //INVALID MOVES, too far
	{
		return ATE_INVALID_MOVE;
	}
	else if (moveOnY == 0 && newX == currX) // moving to the same location
	{
		return ATE_SELF;
	}
	else if (moveOnY == 0) //moving to the same Y
	{
		return ATE_INVALID_MOVE;
	}
	else //probably a valid move not an index problem
	{
		if (abs(moveOnY) > 0 && currX == newX) //moving to the same x
		{
			if (abs(moveOnY) == FIRST_MAX_MOVE && board[currY + moveOnY / FIRST_MAX_MOVE][currX] || board[currY + moveOnY][currX])
			{ //if moving two indexes and a piece interrupts
				return ATE_INVALID_MOVE;
			}
			else //if moving on Y same X
			{
				eatCode = eatChessPiece(newLocation, board);
			}
		}
		else if ((newX == currX - 1 || newX == currX + 1) && board[newY][newX] && abs(moveOnY) == 1)
		{//if different valid x and eating a piece, one Y forward
			eatCode = eatChessPiece(newLocation, board);
		}
		if (eatCode == 0) //if moved
		{
			this->_did_moving = true;
		}
	}
	if (this->getY(this->getLocation()[SOURCE_Y_INDEX]) == MAX_BOARD_INDEX ||
		this->getY(this->getLocation()[SOURCE_Y_INDEX]) == MIN_BOARD_INDEX)
	{
		return 11; // QUEEN CODE
	}
	return eatCode;
}