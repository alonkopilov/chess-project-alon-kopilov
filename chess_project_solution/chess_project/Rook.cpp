#include "Rook.h"

/*
A constructor of Rook, builds new rook.
input: type, color and location.
output: none.
*/
Rook::Rook(const char& type, const char& color, const std::string& location) :  ChessPiece(type, color, location)
{
}

/*
	Moves a Rook based on a given location string
	input: string newLocation (4 LETTERS), pointer to ChessPiece double array (the chess board)
	output: result of eating (or not) code. returns 6 if the move was invalid
*/
int Rook::move(std::string& newLocation, ChessPiece* board[][BOARD_LEN])
{
	int newX = this->getX(newLocation[DEST_X_INDEX]);
	int newY = this->getY(newLocation[DEST_Y_INDEX]);
	int currX = this->getX(this->getLocation()[SOURCE_X_INDEX]);
	int currY = this->getY(this->getLocation()[SOURCE_Y_INDEX]);
	int directionY = 0, directionX = 0, i = 0, eatCode = 0;

	if (newX != currX && newY != currY) //Check if user tries to move diagonally
	{
		return ATE_INVALID_MOVE;
	}
	if (newX == currX) // move vertical
	{
		directionY = (newY - currY) / abs(newY - currY); //Get the Y direction in the board ('-1' up, '1' down)
		for (i = currY + directionY; i != newY; i += directionY)
		{
			if (board[i][currX] != NULL)
			{
				return ATE_INVALID_MOVE;
			}
		}
		eatCode = eatChessPiece(newLocation, board);
	}
	else if (newY == currY) // move horizontal
	{
		directionX = (newX - currX) / abs(newX - currX); //Get the X direction in the board ('-1' left, '1' right)
		for (i = currX + directionX; i != newX; i += directionX)
		{
			if (board[currY][i] != NULL)
			{
				return ATE_INVALID_MOVE;
			}
		}
		eatCode = eatChessPiece(newLocation, board);
	}

	return eatCode;
}