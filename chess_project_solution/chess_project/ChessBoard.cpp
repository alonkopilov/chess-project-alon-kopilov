#include "ChessBoard.h"

/*
	Gets a board string format and constructs a new board by taking every char in the boardFormat and
	allocating a new chess piece by its type
	input: std::string board format
*/
ChessBoard::ChessBoard(const std::string& boardFormat)
{
	int i = 0, j = 0;

	for (i = 0; i < BOARD_SIZE; i++) //Y
	{
		for (j = 0; j < BOARD_SIZE; j++) //X
		{
			_board[i][j] = buildChessPiece(boardFormat[i * BOARD_SIZE + j], j, i);		
		}
	}
}

/*
	Distructs the chess board by deallocating all chess pieces
*/
ChessBoard::~ChessBoard()
{
	int i = 0, j = 0;

	for (i = 0; i < BOARD_SIZE; i++) //Y
	{
		for (j = 0; j < BOARD_SIZE; j++) //X
		{
			if (this->_board[i][j])
			{
				delete _board[i][j];
			}
		}
	}
}

/*
	Build a new chess piece from parameters
	input: char piece char (for example: 'r' for black rook), int X location on the board, int Y location on the board
*/
ChessPiece* ChessBoard::buildChessPiece(char pieceChr, int x, int y)
{
	ChessPiece* piece = NULL;

	switch (std::tolower(pieceChr))
	{
		case KING:
		{
			piece = new King(pieceChr, findPieceColor(pieceChr), buildPieceLocationString(x, y));
			break;
		}
		case QUEEN:
		{
			piece = new Queen(pieceChr, findPieceColor(pieceChr), buildPieceLocationString(x, y));
			break;
		}
		case ROOK:
		{
			piece = new Rook(pieceChr, findPieceColor(pieceChr), buildPieceLocationString(x, y));
			break;
		}
		case KNIGHT:
		{
			piece = new Knight(pieceChr, findPieceColor(pieceChr), buildPieceLocationString(x, y));
			break;
		}
		case BISHOP:
		{
			piece = new Bishop(pieceChr, findPieceColor(pieceChr), buildPieceLocationString(x, y));
			break;
		}
		case PAWN:
		{
			piece = new Pawn(pieceChr, findPieceColor(pieceChr), buildPieceLocationString(x, y));
			break;
		}
		default:
			break;
	}

	return piece;
}

/*
	Returns the color of the chess piece (uppercase piece char = white, lowercase piece char = black)
	output: char piece color
*/
char ChessBoard::findPieceColor(char pieceChr)
{
	return std::isupper(pieceChr) ? WHITE_COLOR_CHAR : BLACK_COLOR_CHAR;
}

/*
	Gets the X and Y location of the piece in the board and builds a string representing the piece location
	input: int X location, int Y location
	output: std::string location string
*/
std::string ChessBoard::buildPieceLocationString(int x, int y)
{
	std::string location = "";
	
	if (x >= 0 && x < BOARD_SIZE && y >= 0 && y < BOARD_SIZE)
	{
		location += x + FIRST_CHAR_ASCII;				//x = a,b,c..
		location += BOARD_SIZE - y + FIRST_INT_ASCII;	//y = 1,2,3..
	}

	return location;
}

int ChessBoard::move(const std::string& location)
{
	std::string moveTo = location;
	int orgX = this->getX(location[SOURCE_X_INDEX]);
	int orgY = this->getY(location[SOURCE_Y_INDEX]);
	int destX = this->getX(location[DEST_X_INDEX]);
	int destY = this->getY(location[DEST_Y_INDEX]);
	int eatCode = 0;
	char turnColor = this->getColor(location.substr(0,2));
	ChessPiece* eatingPiece = this->_board[this->getY(location[SOURCE_Y_INDEX])][this->getX(location[SOURCE_X_INDEX])];
	ChessPiece* eatenPiece = NULL;
	ChessPiece* eatenPieceBackup = NULL;
	ChessPiece* eatingPieceBackup = buildChessPiece(eatingPiece->getType(), // creating a backup piece
		eatingPiece->getX(eatingPiece->getLocation()[SOURCE_X_INDEX]),
		eatingPiece->getY(eatingPiece->getLocation()[SOURCE_Y_INDEX]));
	

	if (!isLocationValid(moveTo)) //checking if the location is valid, indexes
	{
		delete eatingPieceBackup;
		return INVALID_INDEXES;
	}
	if (this->_board[destY][destX]) // save the eaten piece
	{
		eatenPiece = this->_board[destY][destX];
		eatenPieceBackup = buildChessPiece(eatenPiece->getType(), //creating a backup piece for eaten
			eatenPiece->getX(eatenPiece->getLocation()[SOURCE_X_INDEX]),
			eatenPiece->getY(eatenPiece->getLocation()[SOURCE_Y_INDEX]));
	}
	eatCode = eatingPiece->move(moveTo, this->_board); // getting an eatCode move function
	if (eatCode == PAWN_TO_QUEEN) //becomes a queen
	{
		delete eatingPiece; // delete the piece if its becoming a queen
		this->_board[destY][destX] = buildChessPiece(turnColor == BLACK_COLOR_CHAR ? BLACK_QUEEN_CHAR : WHITE_QUEEN_CHAR, destX, destY);
		eatingPiece = this->_board[destY][destX];
		eatCode = 0;
	}

	if (eatCode == ATE_INVALID_MOVE || eatCode == ATE_SELF || eatCode == ATE_KING) 
	{
		if (eatenPieceBackup != NULL)
		{
			delete eatenPieceBackup;
		}
		if (eatingPieceBackup != NULL)
		{
			delete eatingPieceBackup;
		}
		if (ATE_KING) // impossible move, just to make sure
		{
			eatCode = ATE_INVALID_MOVE;
		}
		return eatCode;
	}

	else if (eatCode == ATE_VALID)
	{
		if (isCheckOnColor(turnColor)) //player moved into a check
		{
			if (eatenPieceBackup) //if there was a piece there, original eatenPieceBackup was deleted
			{
				this->_board[destY][destX] = eatenPieceBackup; //move the piece back to place
			} // the original piece was already eating and deleted.
			else
			{
				this->_board[destY][destX] = NULL; //put a null back in place.
			}
			delete eatingPiece;// delete the old piece
			this->_board[orgY][orgX] = eatingPieceBackup; //put the new piece back in place of the old one.
			return ATE_SELF_CHECK;
		}
		else
		{
			if (eatenPieceBackup) //if there is a an eatenpiece backup, we delete it.
			{
				delete eatenPieceBackup;
			}
			delete eatingPieceBackup; // the backup of the eating chess piece. 
		}
		if (isCheckOnColor(turnColor == BLACK_COLOR_CHAR ? WHITE_COLOR_CHAR : BLACK_COLOR_CHAR)) //checking if there was a check made on the other player.
		{
			return ATE_CHECK;
		}
	}
	return ATE_VALID;
}

/*
	Gets a location string and checks if it is a valid location on the board
	input: std::string location
	output: true if the location is valid, else false.
*/
bool ChessBoard::isLocationValid(std::string location)
{
	for (int i = 0; i < 2; i++)
	{ // skipping two indexes so multiplied by 2 // 2 locations in every string
		if (getX(location[i * 2]) > MAX_BOARD_INDEX || getX(location[i * 2]) < MIN_BOARD_INDEX) //if indexes are valid
		{
			return false;
		}
		if (getY(location[i * 2 + 1]) > MAX_BOARD_INDEX || getY(location[i * 2 + 1]) < MIN_BOARD_INDEX) 
		{
			return false;
		}
	}
	return true;
}

/*
	Gets a TWO LETTER location string and checks if there is a piece in that location on the board
	input: std::string location string
	output: the chess piece object in that location
*/
ChessPiece* ChessBoard::playerInLocation(const std::string& location) const
{
	int x = getX(location[SOURCE_X_INDEX]);
	int y = getY(location[SOURCE_Y_INDEX]);

	return this->_board[y][x];
}

/*
	Creates a string representation of the chess board with all pieces
	output: std::string board representation
*/
std::string ChessBoard::printBoard() const
{
	int i = 0, j = 0;
	std::string board = "\n"; // create a new line

	for (i = 0; i < BOARD_SIZE; i++)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			if (_board[i][j] == NULL)
			{
				board += EMPTY_SQUARE;
			}
			else
			{
				board += _board[i][j]->getType();
			}
			board += " ";
		}
		board += "\n";
	}
	return board;
}

/*
	Returns the king piece of a given player color
	input: char player color
	output: king piece of that color
*/
ChessPiece* ChessBoard::getKingToAttack(char color)
{
	for (int i = 0; i < BOARD_LEN; i++)
	{
		for (int j = 0; j < BOARD_LEN;  j++)
		{
			if (this->_board[j][i] && this->_board[j][i]->getColor() == color && ((this->_board[j][i]->getType() == 'k') // if king char
				|| this->_board[j][i]->getType() == 'K')) //checking if the color is valid and if a king using 'k' and 'K'.
			{
				return this->_board[j][i];
			}
		}
	}
	return NULL;
}

/*
	Gets a color of the player and checks if that player is on a Check
	input: char player color
	output: true if the player is on Check, else false
*/
bool ChessBoard::isCheckOnColor(char color)
{
	ChessPiece* kingPiece = this->getKingToAttack(color);

	char attackColor = color == WHITE_COLOR_CHAR ? BLACK_COLOR_CHAR : WHITE_COLOR_CHAR;
	/*
	IF KING ATTACKING KING
	*/
	std::string movingLocation = "";
	for (int i = 0; i < BOARD_LEN; i++)
	{
		for (int j = 0; j < BOARD_LEN; j++)
		{
			if (this->_board[j][i] && this->_board[j][i]->getColor() == attackColor)
			{
				movingLocation = this->_board[j][i]->getLocation();
				movingLocation += kingPiece->getLocation();
				if (this->_board[j][i]->move(movingLocation, this->_board) == ATE_KING)
				{
					return true;
				}
			}
		}
	}
	return false;
}

/*
	Gets a char X location and returns it's representation on the board
	input: char location
	output: int representation on the board
*/
int ChessBoard::getX(char location) const
{
	return location - FIRST_CHAR_ASCII;
}

/*
	Gets a char Y location and returns it's representation on the board
	input: char location
	output: int representation on the board
*/
int ChessBoard::getY(char location) const
{
	return BOARD_SIZE - (location - FIRST_INT_ASCII);
}

/*
	Gets a TWO CHAR location and returns the color of the piece in that location
*/
char ChessBoard::getColor(std::string location) const
{
	ChessPiece* cp = this->_board[getY(location[1])][getX(location[0])];
	return cp->getColor();
}

//turnColor = king's color
/*
bool ChessBoard::isCheckMate(char turnColor)
{
	int validMoves[8][2] = { {1,1},{0,1},{-1,1 },{1,0 },{-1,0},{1,-1},{0,-1},{-1,-1} };
	ChessPiece* kingPiece = getKingToAttack(turnColor);
	ChessPiece* backupPiece = NULL;
	ChessPiece* currPiece = NULL;
	std::string location;
	int x = kingPiece->getX(kingPiece->getLocation()[0]);
	int y = kingPiece->getY(kingPiece->getLocation()[1]);
	if (1)// if king can move
	{
		for (int i = 0; i < 8; i++)
		{
			if (0 <= y + validMoves[i][0] <= 7 && 0 <= x + validMoves[i][1] <= 7)
			{
				currPiece = this->_board[y + validMoves[i][0]][x + validMoves[i][1]];
				if (currPiece == NULL || //if the dest location empty or different than a king and the king's color.
					(currPiece->getType() != 'K' && currPiece->getType() != 'k' && currPiece->getColor() != turnColor))
				{
					location += x + 'a';
					location += y + '0';
					location += x + validMoves[i][1];
					location += y + validMoves[i][0];
					if (currPiece != NULL)
					{
						backupPiece = buildChessPiece(currPiece->getType(), x, y);
						kingPiece->move(location, this->_board);
					}
					else
					{

					}
					
				}
			}
		}
	}
	else if (2) // if attacker can be eaten
	{

	}
	else if (3)// if can block attacker
	{

	}
	else // false cant do anything
	{

	}
}
*/