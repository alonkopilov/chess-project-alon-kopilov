#pragma once

#include "ChessPiece.h"

class Bishop : public ChessPiece
{
public:
	/*
	A constructor of Bishop, builds new Bishop.
	input: type, color and location.
	output: none.
	*/
	Bishop(const char& type, const char& color, const std::string& location);
	/*
	Moves a Bishop based on a given location string
	input: string newLocation (4 LETTERS), pointer to ChessPiece double array (the chess board)
	output: result of eating (or not) code. returns 6 if the move was invalid
	*/
	virtual int move(std::string& newLocation, ChessPiece* board[][BOARD_LEN]);
};

