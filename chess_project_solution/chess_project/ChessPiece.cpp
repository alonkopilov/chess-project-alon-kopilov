#include "ChessPiece.h"
/*
	The function constructs a chesspiece.
	Input:
	The type
	The color
	The location.
	*/
ChessPiece::ChessPiece(const char& type, const char& color, const std::string& location) : _type(type), _color(color), _location(location)
{

}
/*
	The function returns the type of the piece.
	Input:
	None.
	Output:
	The type.
	*/
char ChessPiece::getType() const
{
	return this->_type;
}
/*
	The function returns the color of the piece.
	Input:
	None.
	Output:
	The color.
	*/
char ChessPiece::getColor() const
{
	return this->_color;
}
/*
	The function returns the X value of a char.
	Inptut:
	The char to create the X of.
	Output:
	The X parameter.
	*/
int ChessPiece::getX(char dest) const
{
	return dest - FIRST_CHAR_ASCII;
}
/*
	The function returns the Y value of a char.
	Input:
	The char to create the Y of.
	Output:
	The Y parameter.
	*/
int ChessPiece::getY(char dest) const
{
	return BOARD_LEN - (dest - '0');
}
/*
	The function sets the location of the piece.
	Input:
	The location to set.
	Output:
	None.
	*/
void ChessPiece::setLocation(std::string newLocation)
{
	this->_location = newLocation;
}
/*
	The function returns the location of the piece.
	Input:
	None.
	Output:
	The location of the piece.
	*/
std::string ChessPiece::getLocation() const
{
	return this->_location;
}

/*
	The function represents the eating action of another chess piece, a movement on the board even if it is aimed at a null pointer.
	Input:
	The location to move from and to.
	The board.
	Output:
	The returns value, if the function succeeded and what was the output of it.
	*/
int ChessPiece::eatChessPiece(const std::string location, ChessPiece* board[][BOARD_LEN])
{
	int destX = getX(location[DEST_X_INDEX]);
	int destY = getY(location[DEST_Y_INDEX]);
	int sourceX = getX(location[SOURCE_X_INDEX]);
	int sourceY = getY(location[SOURCE_Y_INDEX]);
	char eatPieceType = 0;
	int eatCode = 0; // EATING INVALID default value
	if (board[destY][destX] != NULL) //Check if there is a piece at dest square
	{
		eatPieceType = board[destY][destX]->getType();
	}
	if (eatPieceType == 'k' || eatPieceType == 'K') //if king, checking the char
	{
		return ATE_KING;
	}
	if (board[destY][destX] != NULL && board[destY][destX]->getColor() != this->_color)
	{
		this->_location = location.substr(2, 2); //taking two chars for the location, the second two chars represent the location.
		delete board[destY][destX]; // delete the old piece.
		board[destY][destX] = NULL;
		eatCode = ATE_VALID;
	}
	else if (board[destY][destX] != NULL) // same color
	{
		return ATE_SELF;
	}
	else if (board[destY][destX] == NULL)
	{
		this->_location = location.substr(2, 2); // second location in the length of 2
		eatCode = ATE_VALID;
	}
	else
	{
		return ATE_INVALID_MOVE;
	}

	board[destY][destX] = board[sourceY][sourceX];
	board[sourceY][sourceX] = NULL;
	return eatCode;
}