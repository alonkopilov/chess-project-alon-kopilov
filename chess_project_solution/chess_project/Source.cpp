/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Game.h"
#include "ChessBoard.h"

#define MAX_MESSAGE_SIZE 1024
#define DEFAULT_CHESS_PLACEMENT "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"

using std::cout;
using std::endl;
using std::string;

void checkSuccessMove(Game& g, int returnCode);
void getReadyToSend(char* str, int sendCode);
int main()
{
	int returnCode = 0;
	string msgFromGraphics = "", ans;
	char msgToGraphics[MAX_MESSAGE_SIZE];

	srand(time_t(NULL));
	
	Pipe p;
	bool isConnect = p.connect();
	
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;
		
		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return 1;
		}
	}

	Game g;

	// SEND BOARD STRING
	strcpy_s(msgToGraphics, DEFAULT_CHESS_PLACEMENT);
	p.sendMessageToGraphics(msgToGraphics);

	cout << g.printGameBoard() << endl;
		
	// FIRST TURN
	msgFromGraphics = p.getMessageFromGraphics();	//get message
	returnCode = g.move(msgFromGraphics);			//try move
	getReadyToSend(msgToGraphics, returnCode);		//get final message to send
	checkSuccessMove(g, returnCode);				//Check if a move was successful: print board + switch turns
	p.sendMessageToGraphics(msgToGraphics);			//Finally send message

	while (msgFromGraphics != "quit")
	{		
		msgFromGraphics = p.getMessageFromGraphics();	//get message
		returnCode = g.move(msgFromGraphics);			//try move
		getReadyToSend(msgToGraphics, returnCode);		//get final message to send
		checkSuccessMove(g, returnCode);				//Check if a move was successful: print board + switch turns
		p.sendMessageToGraphics(msgToGraphics);			//Finally send message		
	}

	p.close();
	return 0;
}

void getReadyToSend(char* str, int sendCode)
{
	std::string s = std::to_string(sendCode);
	char const* pchar = s.c_str();
	strcpy_s(str, MAX_MESSAGE_SIZE, pchar);
}

void checkSuccessMove(Game& g, int returnCode)
{
	if (returnCode == 0 || returnCode == 1 || returnCode == 8)
	{
		g.swapTurns();
		cout << g.printGameBoard() << endl;
	}
}
