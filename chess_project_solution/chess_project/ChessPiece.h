#pragma once
#include <iostream>
#define SOURCE_X_INDEX 0
#define SOURCE_Y_INDEX 1
#define DEST_X_INDEX 2
#define DEST_Y_INDEX 3
#define BLACK_COLOR_CHAR 'b'
#define WHITE_COLOR_CHAR 'w'
#define FIRST_CHAR_ASCII 'a'
#define FIRST_INT_ASCII '0'
#define DESTINATION_SIZE 2
#define BOARD_LEN 8
enum EATING_MESSAGES { ATE_VALID = 0, ATE_CHECK = 1, ATE_SELF = 3, ATE_SELF_CHECK = 4, INVALID_INDEXES = 5, ATE_INVALID_MOVE = 6, PAWN_TO_QUEEN = 11 , ATE_KING = 10};
#define MAX_BOARD_INDEX 7
#define MIN_BOARD_INDEX 0

class ChessPiece
{
public:
	/*
	The function constructs a chesspiece.
	Input:
	The type
	The color
	The location.
	*/
	ChessPiece(const char& type, const char& color, const std::string& location);
	/*
	The function returns the type of the piece.
	Input:
	None.
	Output:
	The type.
	*/
	char getType() const;
	/*
	The function returns the color of the piece.
	Input:
	None.
	Output:
	The color.
	*/
	char getColor() const;
	/*
	The function returns the X value of a char.
	Inptut:
	The char to create the X of.
	Output:
	The X parameter.
	*/
	int getX(char dest) const;
	/*
	The function returns the Y value of a char.
	Input:
	The char to create the Y of.
	Output:
	The Y parameter.
	*/
	int getY(char dest) const;
	/*
	The function sets the location of the piece.
	Input:
	The location to set.
	Output:
	None.
	*/
	void setLocation(std::string newLocation);
	/*
	The function returns the location of the piece.
	Input:
	None.
	Output:
	The location of the piece.
	*/
	std::string getLocation() const;
	/*
	The function represents the move function of the pieces.
	Input:
	The location to move from and to.
	The board.
	Output:
	The returns values of the move function.
	*/
	virtual int move(std::string& newLocation, ChessPiece* board[][BOARD_LEN]) = 0;

protected:
	/*
	The function represents the eating action of another chess piece, a movement on the board even if it is aimed at a null pointer.
	Input:
	The location to move from and to.
	The board.
	Output:
	The returns value, if the function succeeded and what was the output of it.
	*/
	int eatChessPiece(const std::string location, ChessPiece* board[][BOARD_LEN]);
private:
	char _type;
	char _color;
	std::string _location;
};