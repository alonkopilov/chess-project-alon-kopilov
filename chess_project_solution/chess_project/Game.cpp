#include "Game.h"

/*
	Constructs a Chess game by building a Chess board, and setting the starting player
*/
Game::Game()
{
	_board = new ChessBoard(DEFAULT_CHESS_PLACEMENT);
	_turn = WHITE_PLAYER;
}

/*
	Distructs a chess game by deleting the board
*/
Game::~Game()
{
	delete _board;
}

/*
	Gets a FOUR LETTER location string in which the first two characters are the position of the piece we want to move
	and the last two characters are the destination square. The function then performs the move in ChessBoard
	input: std::string four letter location
	output: int move result code
*/
int Game::move(const std::string& location)
{
	if (location.substr(0, 2) == location.substr(2, 2)) // same indexes
	{
		return ATE_MOVE_SAME_INDEX;
	}
	if (_board->playerInLocation(location.substr(0, 2)) == NULL) // first location in is null
	{
		return ATE_NULL_PIECE;
	}
	if (_board->getColor(location) == _turn)
	{
		if (_turn == WHITE_PLAYER)
		{
			return _board->move(location);
		}
		else
		{
			return _board->move(location);
		}
	}
	else
	{
		return ATE_NULL_PIECE;
	}

}

/*
	Swaps the player turns.
*/
void Game::swapTurns()
{
	_turn == BLACK_PLAYER ? _turn = WHITE_PLAYER : _turn = BLACK_PLAYER;
}

/*
	Returns a string of the board representation
	output: std::string board
*/
std::string Game::printGameBoard()
{
	return _board->printBoard();
}
